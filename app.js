const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const keys = require('./config/dev')
const path = require('path');

mongoose.connect(keys.MongoDB.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
    console.log(err);
    if (err) {
        throw console.log('Error: DB not connected');
    } else {
        console.log('DB connected', keys.MongoDB.mongoURI);
    }
});

// logs
app.use(morgan('dev'));


// Setup static assests directory
app.use('/assets', express.static(path.join(__dirname, 'assets')))


app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

// enable CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, enctype");
    next();
});

app.get('/', function (req, res) {
    res.send('Application working');
});

// add routes here

app.use('/user', require('./routes/user'));


const PORT = process.env.PORT || 5000;
app.listen(PORT);

console.log(`Server running on port ${PORT}`)