if (process.env.NODE_ENV === "production") {
    module.exports = require('./prod');
    console.log('Connected on production');
} else {
    module.exports = require('./dev');
    console.log('Connected on dev');
}