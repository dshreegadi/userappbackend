const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String },
    phoneNo: { type: String },
    profileImg: { type: String }
}, { timestamps: true });

const User = mongoose.model('user', userSchema);

module.exports = User;