const UserSchema = require('../models/user');
var mongoose = require('mongoose');
const multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, callback, next) {
        dir = './assets/upload/users';
        callback(null, dir);
    },
    filename: function (req, file, callback) {
        callback(
            null, `user${Date.now()}.png`
        );
    }
});

var upload = multer({
    storage: storage
}).array('user', 1);



module.exports = {

    createUser: async (req, res, next) => {
        console.log(req);
        const { firstName, lastName, email, phoneNo, profileImg } = req.body;

        var filePath = '';
        upload(req, res, async function (err, data) {
            var $firstName = req.body.firstName;
            var $lastName = req.body.lastName;
            var $email = req.body.email;
            var $phoneNo = req.body.phoneNo;
            filePath = req.files[0]['path'];
            const newUser = new UserSchema(
                { firstName: $firstName, lastName: $lastName, email: $email, phoneNo: $phoneNo, profileImg: filePath }
            );
            await newUser.save().
                then((result) => {
                    res.status(200).json({
                        status: true,
                        message: 'success',
                        data: {}
                    })

                })
                .catch((err) => {
                    res.status(500).json({
                        status: false,
                        message: err['errmsg'],
                        data: {}
                    });
                });
        });


    },

    updateUser: async (req, res, next) => {

   

        const obj = { firstName: $firstName, lastName: $lastName, email: $email, phoneNo: $phoneNo, id: id} =  req.body;

console.log(obj);

        const objectId = mongoose.Types.ObjectId;
        UserSchema.updateOne({ _id: objectId(id) }, { $set: obj })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: {}
                })

            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err['errmsg'],
                    data: {}
                });
            });
    },

    deleteUser: async (req, res, next) => {
        const { _id: id } = req.body;
        const objectId = mongoose.Types.ObjectId;

        UserSchema.deleteOne({ _id: objectId(id) })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: {}
                })

            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err['errmsg'],
                    data: {}
                });
            });
    },

    getAllUsers: async (req, res, next) => {
        UserSchema.find({})
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: result
                })
            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err['errmsg'],
                    data: {}
                });
            });
    },

    getUserDetails: async (req, res, next) => {
        const { _id: id } = req.body;
        UserSchema.findOne({ _id: id })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: result
                })
            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err['errmsg'],
                    data: {}
                });
            });
    }
}
