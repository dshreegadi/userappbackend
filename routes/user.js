const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');

router.route('/getAllUsers')
    .get(userController.getAllUsers);

router.route('/getUserDetails')
    .post(userController.getUserDetails);

router.route('/createUser')
    .post(userController.createUser);

router.route('/updateUser')
    .put(userController.updateUser);

router.route('/deleteUser')
    .put(userController.deleteUser);

module.exports = router;